;; Don't ask questions
(defun y (&rest args) t)
(advice-add #'y-or-n-p :before-until #'y)

;; Initialize and install packages
(package-initialize)
(setq package-selected-packages '(org htmlize svg heap))
(push '("melpa" . "https://melpa.org/packages/") package-archives)
(assq-delete-all 'org package--builtins)
(assq-delete-all 'org package--builtin-versions)
(package-refresh-contents)
(package-install-selected-packages)

;; Load packages necessary for export  
(require 'org)
(print (org-version) #'external-debugging-output)
(print (format "(image-type-available-p 'svg): %s" (image-type-available-p 'svg))
       #'external-debugging-output)
(require 'htmlize)
(require 'ox-html)
(require 'svg)
(require 'color)
(advice-add #'org-display-inline-images :before-until #'y)
(setq org-confirm-babel-evaluate nil
      org-export-allow-bind-keywords t
      org-startup-with-inline-images nil
      org-display-remote-inline-images 'skip)
